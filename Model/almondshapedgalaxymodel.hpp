#ifndef ALMONDSHAPEDGALAXYMODEL_HPP
#define ALMONDSHAPEDGALAXYMODEL_HPP

#include <Model/abstractgalaxymodel.hpp>

class AlmondShapedGalaxyModel : public AbstractGalaxyModel
{
    Q_OBJECT

    Q_PROPERTY(double a READ a WRITE setA NOTIFY aChanged)
    Q_PROPERTY(double b READ b WRITE setB NOTIFY bChanged)
    Q_PROPERTY(double angle READ angle WRITE setAngle NOTIFY angleChanged)

    double m_a;
    double m_b;
    double m_angle;

    QList<QPoint> calcPoints() override;

public:
    static void registerQmlType();
    explicit AlmondShapedGalaxyModel(QObject *parent = nullptr);

    double a() const { return m_a; }
    double b() const { return m_b; }
    double angle() const { return m_angle; }

    void setA(double a) { if (!qFuzzyCompare(m_a, a)) emit aChanged(m_a = a); }
    void setB(double b) { if (!qFuzzyCompare(m_b, b)) emit bChanged(m_b = b); }
    void setAngle(double angle) { if (!qFuzzyCompare(m_angle, angle)) emit angleChanged(m_angle = angle); }

signals:
    void aChanged(double a);
    void bChanged(double b);
    void angleChanged(double angle);
};

#endif // ALMONDSHAPEDGALAXYMODEL_HPP
