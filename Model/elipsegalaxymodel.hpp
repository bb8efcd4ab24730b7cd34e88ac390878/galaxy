#ifndef ELIPSEGALAXYMODEL_HPP
#define ELIPSEGALAXYMODEL_HPP

#include <Model/abstractgalaxymodel.hpp>

class ElipseGalaxyModel : public AbstractGalaxyModel
{
    Q_OBJECT

    QList<QPoint> calcPoints() override;

    Q_PROPERTY(double radius READ radius WRITE setRadius NOTIFY radiusChanged)

    double m_radius = 0;

public:
    static void registerQmlType();
    explicit ElipseGalaxyModel(QObject *parent = nullptr);
    double radius() const { return m_radius; }
    void setRadius(double radius) { if (!qFuzzyCompare(m_radius, radius)) emit radiusChanged(m_radius = radius); }

signals:
    void radiusChanged(double radius);
};

#endif // ELIPSEGALAXYMODEL_HPP
