#include "spiralgalaxymodel.hpp"

#include <GalaxyGenerator/galaxygenerator.h>

#include <QQmlEngine>

QList<QPoint> SpiralGalaxyModel::calcPoints()
{ return generateSpiralGalaxy(static_cast<int>(width()), static_cast<int>(height()), l(), h(), a(), arms()); }

void SpiralGalaxyModel::registerQmlType() { qmlRegisterType<SpiralGalaxyModel>("GalaxyModel", 1, 0, "SpiralGalaxyModel"); }

SpiralGalaxyModel::SpiralGalaxyModel(QObject *parent) : AbstractGalaxyModel(parent)
{
    connect(this, &SpiralGalaxyModel::lChanged, this, &AbstractGalaxyModel::update);
    connect(this, &SpiralGalaxyModel::hChanged, this, &AbstractGalaxyModel::update);
    connect(this, &SpiralGalaxyModel::aChanged, this, &AbstractGalaxyModel::update);
    connect(this, &SpiralGalaxyModel::armsChanged, this, &AbstractGalaxyModel::update);
}
