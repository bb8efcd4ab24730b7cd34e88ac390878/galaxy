#include "abstractgalaxymodel.hpp"

void AbstractGalaxyModel::updateOnRadiusChanged()
{
    double radius = width() > height() ? height() : width();
    if (!qFuzzyCompare(radius, this->radius)) update();
}

void AbstractGalaxyModel::update()
{
    auto points = calcPoints();
    if (m_points != points) emit pointsChanged(m_points = points);
}

AbstractGalaxyModel::AbstractGalaxyModel(QObject *parent) : QObject(parent)
{
    connect(this, &AbstractGalaxyModel::widthChanged, this, &AbstractGalaxyModel::updateOnRadiusChanged);
    connect(this, &AbstractGalaxyModel::heightChanged, this, &AbstractGalaxyModel::updateOnRadiusChanged);
}
