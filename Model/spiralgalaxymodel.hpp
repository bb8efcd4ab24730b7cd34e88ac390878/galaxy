#ifndef SPIRALGALAXYMODEL_HPP
#define SPIRALGALAXYMODEL_HPP

#include <Model/abstractgalaxymodel.hpp>

class SpiralGalaxyModel : public AbstractGalaxyModel
{
    Q_OBJECT

    Q_PROPERTY(double l READ l WRITE setL NOTIFY lChanged)
    Q_PROPERTY(double h READ h WRITE setH NOTIFY hChanged)
    Q_PROPERTY(double a READ a WRITE setA NOTIFY aChanged)
    Q_PROPERTY(int arms READ arms WRITE setArms NOTIFY armsChanged)

    double m_l;
    double m_h;
    double m_a;
    int m_arms;

    QList<QPoint> calcPoints() override;

public:
    static void registerQmlType();
    explicit SpiralGalaxyModel(QObject *parent = nullptr);

    double l() const { return m_l; }
    double h() const { return m_h; }
    double a() const { return m_a; }
    int arms() const { return m_arms; }

    void setL(double l) { if (!qFuzzyCompare(m_l, l)) emit lChanged(m_l = l); }
    void setH(double h) { if (!qFuzzyCompare(m_h, h)) emit hChanged(m_h = h); }
    void setA(double a) { if (!qFuzzyCompare(m_a, a)) emit aChanged(m_a = a); }
    void setArms(int arms) { if (m_arms != arms) emit armsChanged(m_arms = arms); }

signals:
    void lChanged(double l);
    void hChanged(double h);
    void aChanged(double a);
    void armsChanged(int arms);
};

#endif // SPIRALGALAXYMODEL_HPP
