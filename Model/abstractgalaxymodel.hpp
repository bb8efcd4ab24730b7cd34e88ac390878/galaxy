#ifndef ABSTRACTGALAXYMODEL_HPP
#define ABSTRACTGALAXYMODEL_HPP

#include <QObject>
#include <QList>
#include <QPointF>

class AbstractGalaxyModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QList<QPoint> points READ points NOTIFY pointsChanged)
    Q_PROPERTY(double width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(double height READ height WRITE setHeight NOTIFY heightChanged)

    QList<QPoint> m_points;
    double m_width;
    double m_height;

    double radius = 0;
    void updateOnRadiusChanged();


protected:
    virtual QList<QPoint> calcPoints() = 0;

public:
    explicit AbstractGalaxyModel(QObject *parent = nullptr);

    void update();

    QList<QPoint> points() { return m_points; }
    double width() const { return m_width; }
    double height() const { return m_height; }

    void setWidth(double width) { if (!qFuzzyCompare(m_width, width)) emit widthChanged(m_width = width); }
    void setHeight(double height) { if (!qFuzzyCompare(m_height, height)) emit heightChanged(m_height = height); }

signals:
    void pointsChanged(QList<QPoint> points);
    void widthChanged(double width);
    void heightChanged(double height);
};

#endif // ABSTRACTGALAXYMODEL_HPP
