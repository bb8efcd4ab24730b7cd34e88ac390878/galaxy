#include "almondshapedgalaxymodel.hpp"

#include <GalaxyGenerator/galaxygenerator.h>

#include <QQmlEngine>

QList<QPoint> AlmondShapedGalaxyModel::calcPoints()
{ return generateAlmondShapedGalaxy(static_cast<int>(width()), static_cast<int>(height()), a(), b(), angle()); }

void AlmondShapedGalaxyModel::registerQmlType() { qmlRegisterType<AlmondShapedGalaxyModel>("GalaxyModel", 1, 0, "AlmondShapedGalaxyModel"); }

AlmondShapedGalaxyModel::AlmondShapedGalaxyModel(QObject *parent) : AbstractGalaxyModel(parent)
{
    connect(this, &AlmondShapedGalaxyModel::aChanged, this, &AbstractGalaxyModel::update);
    connect(this, &AlmondShapedGalaxyModel::bChanged, this, &AbstractGalaxyModel::update);
    connect(this, &AlmondShapedGalaxyModel::angleChanged, this, &AbstractGalaxyModel::update);
}
