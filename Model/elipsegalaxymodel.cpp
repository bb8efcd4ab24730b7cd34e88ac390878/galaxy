#include "elipsegalaxymodel.hpp"

#include <GalaxyGenerator/galaxygenerator.h>

#include <QQmlEngine>

QList<QPoint> ElipseGalaxyModel::calcPoints()
{ return generateElipseGalaxy(static_cast<int>(width()), static_cast<int>(height()), radius()); }

void ElipseGalaxyModel::registerQmlType() { qmlRegisterType<ElipseGalaxyModel>("GalaxyModel", 1, 0, "ElipseGalaxyModel"); }

ElipseGalaxyModel::ElipseGalaxyModel(QObject *parent) : AbstractGalaxyModel(parent)
{
    connect(this, &ElipseGalaxyModel::radiusChanged, this, &AbstractGalaxyModel::update);
}
