import QtQuick 2.9
import QtQuick.Window 2.11
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.4

import "Page"

import Galaxy 1.0
import GalaxyModel 1.0
import AppSettings 1.0

ApplicationWindow {
    id: window
    visible: true
//    x: Screen.desktopAvailableWidth / 2
    visibility: AppSettings.fullScreen ? Window.FullScreen : Window.AutomaticVisibility
    width: 560
    height: 960

    Material.theme: Material.Dark

    header: TabBar {
        id: tabBar
        currentIndex: view.currentIndex

        Repeater {
            model: [qsTr("Elipse galaxy"), qsTr("Almond-shaped galaxy"), qsTr("Spiral galaxy"), qsTr("Options")]
            delegate: TabButton {
                text: modelData
                width: implicitWidth
            }
        }
    }

    SwipeView {
        id: view
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        onCurrentIndexChanged: {
            for (var i = 0; i < 3; i++) {
                view.itemAt(i).stopAnimation()
            }
        }

        ElipseGalaxyPage {}

        AlmondShapedGalaxyPage {}

        SpiralGalaxyPage {}

        OptionsPage {}
    }

    ToolButton {
        padding: Screen.pixelDensity * 3
        font.family: "fontello"
        font.pointSize: 20
        text: view.currentItem.animationRunning ? "\u0023" : "\u0022"
        x: parent.width - width - Screen.pixelDensity * 3
        y: Screen.pixelDensity * 3
        visible: view.currentIndex < 3
        onClicked: {
            if (view.currentItem.animationRunning) {
                view.currentItem.stopAnimation()
            } else {
                view.currentItem.startAnimation()
            }
        }
    }
}
