import QtQuick 2.9
import QtQuick.Window 2.11
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.4

import Galaxy 1.0
import GalaxyModel 1.0
import AppSettings 1.0

Page {
    id: page

    readonly property bool animationRunning: animation1.running || animation2.running

    function stopAnimation() {
        animation1.stop()
        animation2.stop()
        sliderR.enabled = true
    }

    function startAnimation() {
        sliderR.enabled = false
        animation1.start()
    }

    Column {
        anchors.fill: parent

        Item {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            height: parent.height - sliderR.height

            Image {
                anchors.fill: parent
                fillMode: Image.PreserveAspectCrop
                source: AppSettings.lastImageSource
            }

            Galaxy {
                id: galaxy
                anchors.fill: parent
                background: AppSettings.background === AppSettings.Transparent ? "transparent" : Material.color(AppSettings.background)
                starColor: AppSettings.starColor === AppSettings.Transparent ? "transparent" : Material.color(AppSettings.starColor)
                model: ElipseGalaxyModel {
                    radius: sliderR.value
                    width: galaxy.width
                    height: galaxy.height
                }
            }
        }

        Row {
            width: parent.width
            Label {
                id: rLabel
                anchors.verticalCenter: parent.verticalCenter
                padding: Screen.pixelDensity
                text: ("R: " + sliderR.value).slice(0, 8)
            }

            Slider {
                id: sliderR
                from: 1
                to: galaxy.width > galaxy.height ? galaxy.height : galaxy.width
                value: to*0.7
                width: parent.width - rLabel.width
            }
        }
    }

    PropertyAnimation {
        id: animation1
        duration: (AppSettings.animationDuration * 1000) / 2
        target: sliderR
        property: "value"
        from: sliderR.from
        to: sliderR.to
        onStopped: if (sliderR.value === sliderR.to) animation2.start()
    }

    PropertyAnimation {
        id: animation2
        duration: (AppSettings.animationDuration * 1000) / 2
        target: sliderR
        property: "value"
        from: animation1.to
        to: animation1.from
        onStopped: if (sliderR.value === sliderR.from) animation1.start()
    }
}
