import QtQuick 2.9
import QtQuick.Window 2.11
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.4

import Galaxy 1.0
import GalaxyModel 1.0
import AppSettings 1.0

Page {
    id: page

    readonly property bool animationRunning: animation1.running || animation2.running

    function stopAnimation() {
        animation1.stop()
        animation2.stop()
        sliderAngle.enabled = true
    }

    function startAnimation() {
        sliderAngle.enabled = false
        animation1.start()
    }

    Column {
        anchors.fill: parent

        Item {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            height: parent.height - sliderA.height - sliderB.height - sliderAngle.height

            Image {
                anchors.fill: parent
                fillMode: Image.PreserveAspectCrop
                source: AppSettings.lastImageSource
            }

            Galaxy {
                id: galaxy
                anchors.fill: parent
                background: AppSettings.background === AppSettings.Transparent ? "transparent" : Material.color(AppSettings.background)
                starColor: AppSettings.starColor === AppSettings.Transparent ? "transparent" : Material.color(AppSettings.starColor)
                model: AlmondShapedGalaxyModel {
                    width: galaxy.width
                    height: galaxy.height
                    a: sliderA.value
                    b: sliderB.value
                    angle: sliderAngle.value
                }
            }
        }

        Row {
            width: parent.width
            Label {
                id: aLabel
                anchors.verticalCenter: parent.verticalCenter
                padding: Screen.pixelDensity
                text: ("A: " + sliderA.value).slice(0, 8)
            }

            Slider {
                id: sliderA
                value: 0.7
                width: parent.width - aLabel.width
            }
        }

        Row {
            width: parent.width
            Label {
                id: bLabel
                anchors.verticalCenter: parent.verticalCenter
                padding: Screen.pixelDensity
                text: ("B: " + sliderB.value).slice(0, 8)
            }

            Slider {
                id: sliderB
                value: 0.2
                width: parent.width - bLabel.width
            }
        }

        Row {
            width: parent.width
            Label {
                id: angleLabel
                anchors.verticalCenter: parent.verticalCenter
                padding: Screen.pixelDensity
                text: ("Angle: " + sliderAngle.value).slice(0, 12)
            }

            Slider {
                id: sliderAngle
                to: Math.PI
                value: to / 4
                width: parent.width - angleLabel.width
            }
        }
    }

    PropertyAnimation {
        id: animation1
        duration: (AppSettings.animationDuration * 1000) / 2
        target: sliderAngle
        property: "value"
        from: sliderAngle.from
        to: sliderAngle.to
        onStopped: if (sliderAngle.value === sliderAngle.to) animation2.start()
    }

    PropertyAnimation {
        id: animation2
        duration: (AppSettings.animationDuration * 1000) / 2
        target: sliderAngle
        property: "value"
        from: animation1.to
        to: animation1.from
        onStopped: if (sliderAngle.value === sliderAngle.from) animation1.start()
    }
}
