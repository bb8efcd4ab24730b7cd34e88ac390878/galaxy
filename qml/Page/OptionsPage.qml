import QtQuick 2.9
import QtQuick.Window 2.11
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.4

import AppSettings 1.0

Page {
    id: page

    padding: Screen.pixelDensity * 2
    rightPadding: 0

    contentItem: ScrollView {
        contentWidth: -1

        Column {
            spacing: Screen.pixelDensity * 3

            CheckBox {
                checked: AppSettings.fullScreen
                onClicked: AppSettings.fullScreen = checked
                text: qsTr("Full screen")
                width: parent.width
            }

            Row {
                width: parent.width
                Label {
                    id: durationLabel
                    anchors.verticalCenter: parent.verticalCenter
                    padding: Screen.pixelDensity
                    text: qsTr("Animation dutation (seconds)")
                }

                SpinBox {
                    width: parent.width - durationLabel.width
                    from: 1
                    to: 30
                    onValueChanged: AppSettings.animationDuration = value
                    value: AppSettings.animationDuration
                }
            }

            GroupBox {
                title: qsTr("Star color")
                width: page.width - Screen.pixelDensity * 4

                contentItem: Flow {

                    Repeater {
                        model: [
                            Material.Red,
                            Material.Pink,
                            Material.Purple,
                            Material.DeepPurple,
                            Material.Indigo,
                            Material.Blue,
                            Material.LightBlue,
                            Material.Cyan,
                            Material.Teal,
                            Material.Green,
                            Material.LightGreen,
                            Material.Lime,
                            Material.Yellow,
                            Material.Amber,
                            Material.Orange,
                            Material.DeepOrange,
                            Material.Brown,
                            Material.Grey,
                            Material.BlueGrey
                        ]

                        delegate: RoundButton {
                            Material.background: Material.color(modelData)
                            font.family: "fontello"
                            font.pointSize: 20
                            text: modelData === AppSettings.starColor ? "\u0021" : ""
                            onClicked: AppSettings.starColor = modelData
                        }
                    }
                }
            }

            GroupBox {
                width: page.width - Screen.pixelDensity * 4

                label: CheckBox {
                    id: backgroundCheck
                    checked: AppSettings.background !== AppSettings.Transparent
                    onClicked: checked ? AppSettings.background = AppSettings.Red : AppSettings.background = AppSettings.Transparent
                    enabled: !imageCheck.checked
                    text: qsTr("Background color")
                }

                contentItem: Flow {
                    enabled: backgroundCheck.checked

                    Repeater {
                        model: [
                            Material.Red,
                            Material.Pink,
                            Material.Purple,
                            Material.DeepPurple,
                            Material.Indigo,
                            Material.Blue,
                            Material.LightBlue,
                            Material.Cyan,
                            Material.Teal,
                            Material.Green,
                            Material.LightGreen,
                            Material.Lime,
                            Material.Yellow,
                            Material.Amber,
                            Material.Orange,
                            Material.DeepOrange,
                            Material.Brown,
                            Material.Grey,
                            Material.BlueGrey
                        ]

                        delegate: RoundButton {
                            Material.background: Material.color(modelData)
                            font.family: "fontello"
                            font.pointSize: 20
                            text: modelData === AppSettings.background ? "\u0021" : ""
                            onClicked: AppSettings.background = modelData
                        }
                    }
                }
            }

            GroupBox {
                width: page.width - Screen.pixelDensity * 4

                label: CheckBox {
                    id: imageCheck
                    checked: AppSettings.lastImage > -1
                    onClicked: checked ? AppSettings.lastImage = 0 : AppSettings.lastImage = -1
                    text: qsTr("Background image")
                }

                contentItem: Flow {
                    id: imageFlow
                    enabled: imageCheck.checked
                    spacing: Screen.pixelDensity

                    Repeater {
                        model: AppSettings.images()

                        delegate: Image {
                            asynchronous: true
                            cache: true
                            fillMode: Image.PreserveAspectCrop
                            source: modelData
                            width: (imageFlow.width -  Screen.pixelDensity * 2) / 3
                            height: width

                            RadioButton {
                                checked: index === AppSettings.lastImage
                                onClicked: AppSettings.lastImage = index
                            }
                        }
                    }
                }
            }
        }
    }
}
