import QtQuick 2.9
import QtQuick.Window 2.11
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.4

import Galaxy 1.0
import GalaxyModel 1.0
import AppSettings 1.0

Page {
    id: page

    readonly property bool animationRunning: animation1.running || animation2.running

    function stopAnimation() {
        animation1.stop()
        animation2.stop()
        sliderAngle.enabled = true
    }

    function startAnimation() {
        sliderAngle.enabled = false
        animation1.start()
    }

    Column {
        anchors.fill: parent

        Item {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            height: parent.height - sliderL.height - sliderH.height - sliderAngle.height - spinArm.height

            Image {
                anchors.fill: parent
                fillMode: Image.PreserveAspectCrop
                source: AppSettings.lastImageSource
            }

            Galaxy {
                id: galaxy
                anchors.fill: parent
                background: AppSettings.background === AppSettings.Transparent ? "transparent" : Material.color(AppSettings.background)
                starColor: AppSettings.starColor === AppSettings.Transparent ? "transparent" : Material.color(AppSettings.starColor)
                model: SpiralGalaxyModel {
                    width: galaxy.width
                    height: galaxy.height
                    l: sliderL.value
                    h: sliderH.value
                    a: sliderAngle.value
                    arms: spinArm.value
                }
            }
        }

        Row {
            width: parent.width
            Label {
                id: lLabel
                anchors.verticalCenter: parent.verticalCenter
                padding: Screen.pixelDensity
                text: ("L: " + sliderL.value).slice(0, 8)
            }

            Slider {
                id: sliderL
                from: 1
                to: galaxy.width
                value: to
                width: parent.width - lLabel.width
            }
        }

        Row {
            width: parent.width
            Label {
                id: hLabel
                anchors.verticalCenter: parent.verticalCenter
                padding: Screen.pixelDensity
                text: ("H: " + sliderH.value).slice(0, 8)
            }

            Slider {
                id: sliderH
                from: 1
                to: galaxy.width / 4
                value: to*0.05
                width: parent.width - hLabel.width
            }
        }

        Row {
            width: parent.width
            Label {
                id: aLabel
                anchors.verticalCenter: parent.verticalCenter
                padding: Screen.pixelDensity
                text: ("A: " + sliderAngle.value).slice(0, 8)
            }

            Slider {
                id: sliderAngle
                from: -Math.PI*3
                to: Math.PI*3
                value: Math.PI
                width: parent.width - aLabel.width
            }
        }

        Row {
            width: parent.width

            Label {
                id: armLabel
                anchors.verticalCenter: parent.verticalCenter
                padding: Screen.pixelDensity
                text: "Arms"
            }

            SpinBox {
                id: spinArm
                from: 1
                to: 10
                value: 3
                width: parent.width - armLabel.width
            }
        }
    }

    PropertyAnimation {
        id: animation1
        duration: (AppSettings.animationDuration * 1000) / 2
        target: sliderAngle
        property: "value"
        easing.type: Easing.OutInQuad
        from: sliderAngle.from
        to: sliderAngle.to
        onStopped: if (sliderAngle.value === sliderAngle.to) animation2.start()
    }

    PropertyAnimation {
        id: animation2
        duration: (AppSettings.animationDuration * 1000) / 2
        target: sliderAngle
        property: "value"
        easing.type: animation1.easing.type
        from: animation1.to
        to: animation1.from
        onStopped: if (sliderAngle.value === sliderAngle.from) animation1.start()
    }
}
