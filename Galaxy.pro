#-------------------------------------------------
#
# Project created by QtCreator 2019-02-14T12:14:37
#
#-------------------------------------------------

QT       += core gui quick

TARGET = Galaxy
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
    GalaxyGenerator/elipsegalaxy_impl.cpp \
    GalaxyGenerator/spiralgalaxy_impl.cpp \
    GalaxyGenerator/almondshapedgalaxy_impl.cpp \
    Model/abstractgalaxymodel.cpp \
    galaxy.cpp \
    Model/spiralgalaxymodel.cpp \
    Model/almondshapedgalaxymodel.cpp \
    Model/elipsegalaxymodel.cpp \
    Utils/Settings/settings.cpp \
    Settings/appsettings.cpp

HEADERS += \
    GalaxyGenerator/galaxygenerator.h \
    Model/abstractgalaxymodel.hpp \
    galaxy.hpp \
    Model/spiralgalaxymodel.hpp \
    Model/almondshapedgalaxymodel.hpp \
    Model/elipsegalaxymodel.hpp \
    Utils/Settings/settings.hpp \
    Utils/Singleton/singleton.hpp \
    Settings/appsettings.hpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    qml.qrc \
    media.qrc
