#include <QFontDatabase>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <galaxy.hpp>
#include <Model/elipsegalaxymodel.hpp>
#include <Model/almondshapedgalaxymodel.hpp>
#include <Model/spiralgalaxymodel.hpp>
#include <Settings/appsettings.hpp>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    Galaxy::registerQmlType();
    ElipseGalaxyModel::registerQmlType();
    AlmondShapedGalaxyModel::registerQmlType();
    SpiralGalaxyModel::registerQmlType();
    AppSettings::registerQmlType();

    QFontDatabase fontDatabase;
    if (fontDatabase.addApplicationFont(":/fonts/fontello/font/fontello.ttf") == -1)
        qWarning() << "Failed to load fontello.ttf";

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();

}
