#ifndef GALAXY_HPP
#define GALAXY_HPP

#include <QQuickPaintedItem>
#include <Model/abstractgalaxymodel.hpp>

class Galaxy : public QQuickPaintedItem
{
    Q_OBJECT

    void paint(QPainter *painter) override;

    Q_PROPERTY(QObject *model READ model WRITE setModel NOTIFY modelChanged)
    Q_PROPERTY(QColor background READ background WRITE setBackground NOTIFY backgroundChanged)
    Q_PROPERTY(QColor starColor READ starColor WRITE setStarColor NOTIFY starColorChanged)

    AbstractGalaxyModel *m_model;
    QColor m_background = Qt::transparent;
    QColor m_starColor = "white";

public:
    static void registerQmlType();
    Galaxy(QQuickItem *parent = nullptr);

    void update() { QQuickPaintedItem::update(); }

    QObject *model() const { return m_model; }
    QColor background() const { return m_background; }
    QColor starColor() const { return m_starColor; }

    void setModel(QObject *model)
    {
        if (m_model != model)
        {
            m_model = static_cast<AbstractGalaxyModel*>(model);
            m_model->setParent(this);
            connect(m_model, &AbstractGalaxyModel::pointsChanged, this, &Galaxy::update);
            emit modelChanged(m_model);
        }
    }
    void setBackground(QColor background) { if (m_background != background) emit backgroundChanged(m_background = background); }
    void setStarColor(QColor starColor) { if (m_starColor != starColor) emit starColorChanged(m_starColor = starColor); }

signals:
    void pointsChanged(QList<QPoint> points);
    void backgroundChanged(QColor background);
    void starColorChanged(QColor starColor);
    void modelChanged(QObject *model);
};

#endif // GALAXY_HPP
