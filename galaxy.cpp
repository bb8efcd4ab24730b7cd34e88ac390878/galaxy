#include "galaxy.hpp"

#include <QPainter>
#include <QQmlEngine>
#include <QDebug>
#include <math.h>

#include <Settings/appsettings.hpp>

void Galaxy::paint(QPainter *painter)
{
    painter->setRenderHint(QPainter::Antialiasing);
    painter->fillRect(0, 0, static_cast<int>(width()), static_cast<int>(height()), background());
    painter->setPen(starColor());
    foreach (auto val, m_model->points()) painter->drawPoint(QPointF(val.x() + width() / 2, val.y() + height() / 2));
}

void Galaxy::registerQmlType() { qmlRegisterType<Galaxy>("Galaxy", 1, 0, "Galaxy"); }

Galaxy::Galaxy(QQuickItem *parent)
    : QQuickPaintedItem(parent)
{
    connect(this, &Galaxy::pointsChanged, this, &Galaxy::update);
    connect(this, &Galaxy::backgroundChanged, this, &Galaxy::update);
    connect(this, &Galaxy::starColorChanged, this, &Galaxy::update);
}
