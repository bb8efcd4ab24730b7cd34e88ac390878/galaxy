#include "appsettings.hpp"

#include <QQmlEngine>
#include <QDebug>

Singleton<AppSettings> settings;

QStringList images = {
    "qrc:/media/1.jpg",
    "qrc:/media/2.jpg",
    "qrc:/media/3.jpg",
    "qrc:/media/4.jpg",
    "qrc:/media/5.jpg",
    "qrc:/media/6.jpg"
};

void AppSettings::registerQmlType()
{
    qmlRegisterSingletonType<AppSettings>("AppSettings", 1, 0, "AppSettings",
                [](QQmlEngine *engine, QJSEngine *scriptEngine) -> QObject * {
                    Q_UNUSED(engine)
                    Q_UNUSED(scriptEngine)

                    return settings.instance();
                });
}

AppSettings::AppSettings(QObject *parent) : Settings(parent) { read(); }

QString AppSettings::lastImageSource() const { return lastImage() > -1 && lastImage() < ::images.size() ? ::images.at(lastImage()) : QString(); }

QStringList AppSettings::images() const { return ::images; }
