#ifndef APPSETTINGS_HPP
#define APPSETTINGS_HPP

#include <Utils/Settings/settings.hpp>
#include <Utils/Singleton/singleton.hpp>

class AppSettings : public Settings
{
    Q_OBJECT

public:

    enum Colors
    {
        Red,
        Pink,
        Purple,
        DeepPurple,
        Indigo,
        Blue,
        LightBlue,
        Cyan,
        Teal,
        Green,
        LightGreen,
        Lime,
        Yellow,
        Amber,
        Orange,
        DeepOrange,
        Brown,
        Grey,
        BlueGrey,
        Transparent
    };
    Q_ENUM(Colors)

private:
    Q_PROPERTY(Colors background READ background WRITE setBackground NOTIFY backgroundChanged)
    Q_PROPERTY(Colors starColor READ starColor WRITE setStarColor NOTIFY starColorChanged)
    Q_PROPERTY(int lastImage READ lastImage WRITE setLastImage NOTIFY lastImageChanged)
    Q_PROPERTY(bool fullScreen READ fullScreen WRITE setFullScreen NOTIFY fullScreenChanged)
    Q_PROPERTY(QString lastImageSource READ lastImageSource NOTIFY lastImageSourceChanged)
    Q_PROPERTY(int animationDuration READ animationDuration WRITE setAnimationDuration NOTIFY animationDurationChanged)

    Colors m_background = Transparent;
    Colors m_starColor = Cyan;
    int m_lastImage = -1;
    bool m_fullScreen = true;
    int m_animationDuration = 8;

public:
    static void registerQmlType();
    explicit AppSettings(QObject *parent = nullptr);

    Colors background() const { return m_background; }
    Colors starColor() const { return m_starColor; }
    int lastImage() const { return m_lastImage; }
    bool fullScreen() const { return m_fullScreen; }
    QString lastImageSource() const;
    int animationDuration() const { return m_animationDuration; }

    void setBackground(Colors backgroud) { if (m_background != backgroud) emit backgroundChanged(m_background = backgroud); }
    void setStarColor(Colors starColor) { if (m_starColor != starColor) emit starColorChanged(m_starColor = starColor); }
    void setLastImage(int lastImage)
    {
        if (m_lastImage != lastImage)
        {
            emit lastImageChanged(m_lastImage = lastImage);
            emit lastImageSourceChanged(lastImageSource());
            setBackground(Colors::Transparent);
        }
    }
    void setFullScreen(bool fullScreen) { if (m_fullScreen != fullScreen) emit fullScreenChanged(m_fullScreen = fullScreen); }
    void setAnimationDuration(int animationDuration) { if (m_animationDuration != animationDuration) emit animationDurationChanged(m_animationDuration = animationDuration); }

public slots:
    QStringList images() const;

signals:
    void backgroundChanged(Colors background);
    void starColorChanged(Colors starColor);
    void lastImageChanged(int lastImage);
    void fullScreenChanged(bool fullScreen);
    void lastImageSourceChanged(QString lastImageSource);
    void animationDurationChanged(int animationDuration);
};

extern Singleton<AppSettings> settings;

extern QStringList images;

#endif // APPSETTINGS_HPP
