#include "galaxygenerator.h"

#include <math.h>
#include <QRandomGenerator>

static inline double f(double d, double r)
{
    d *= 0.15;
    return exp( ( -(d*d*d) / (r*r) ) );
}

static inline QPoint rot(double x, double y, double a)
{
    return QPointF(x*cos(a) + y*sin(a), -x*sin(a) + y*cos(a)).toPoint();
}

QList<QPoint> generateSpiralGalaxy(int width, int height, double l, double h, double angle, int arms)
{
    int radius = width > height ? height : width;
    const double b = angle * 0.2 * radius;
    radius = static_cast<int>(pow(radius*radius + radius*radius, 0.5) / 2);

    QRandomGenerator rnd;

    QList<QPoint> points;
    for (int x = -radius; x < radius; x++)
    {
        for (int y = -radius; y < radius; y++)
        {
            if (rnd.generateDouble()*2.5 <= f(x*x, h*h) * f(y*y, l*l))
            {
                double d = pow(x*x + y*y, 0.5);
                points << rot(x, y, b/d);
            }
        }
    }

    double al = M_PI / arms;
    foreach (auto p, QList<QPoint>(points))
    {
        for (int i = 1; i < arms; i++)
        {
            points << rot(p.x(), p.y(), al*i);
        }
    }

    return points;
}
