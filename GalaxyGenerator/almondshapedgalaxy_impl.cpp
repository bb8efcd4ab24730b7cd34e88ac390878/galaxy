#include "galaxygenerator.h"

#include <math.h>
#include <QRandomGenerator>

static inline QPoint rot(double x, double y, double a)
{
    return QPointF(x*cos(a) + y*sin(a), -x*sin(a) + y*cos(a)).toPoint();
}

static inline double f(double p) { return exp( -p ); }

QList<QPoint> generateAlmondShapedGalaxy(int width, int height, double a, double b, double angle)
{
    int radius = width > height ? width : height;
    a = a * radius / 2.5;
    b = b * radius / 2.5;
    radius = static_cast<int>(pow(radius*radius + radius*radius, 0.5)/2);

    QRandomGenerator rnd;

    QList<QPoint> points;
    for (int x = -radius; x < radius; x++)
    {
        for (int y = -radius; y < radius; y++)
        {
            double p = (x*x) / (a*a) + (y*y) / (b*b);
            if (rnd.generateDouble()*2 <= f(p)) points << rot(x, y, angle);
        }
    }

    return points;
}
