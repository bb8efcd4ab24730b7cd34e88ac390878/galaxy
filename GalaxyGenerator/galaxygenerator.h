#ifndef GALAXYGENERATOR_H
#define GALAXYGENERATOR_H

#include <qvector.h>
#include <qpoint.h>

extern QList<QPoint> generateElipseGalaxy(int width, int height, double radius);

extern QList<QPoint> generateAlmondShapedGalaxy(int width, int height, double a, double b, double angle);

extern QList<QPoint> generateSpiralGalaxy(int width, int height, double l, double h, double angle, int arms);

#endif // GALAXYGENERATOR_H
