#include "galaxygenerator.h"

#include <math.h>
#include <QRandomGenerator>

static inline double f(double d, double r)
{
    d *= 0.35;
    return exp( ( -(d*d*d) / (r*r) ) );
}

QList<QPoint> generateElipseGalaxy(int width, int height, double radius)
{
    width /= 2;
    height /= 2;

    QRandomGenerator rnd;

    QList<QPoint> points;
    for (int x = -width; x < width; x++)
    {
        for (int y = -height; y < height; y++)
        {
            double d = pow(x*x + y*y, 0.5);
            if (rnd.generateDouble()*2 <= f(d, radius)) points << QPoint(x, y);
        }
    }

    return points;
}
