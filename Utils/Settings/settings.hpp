#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <QSettings>

class Settings : public QSettings
{
    Q_OBJECT

    bool m_syncEnable = true;

public:
    explicit Settings(QObject *parent = nullptr);

    bool syncEnable() const;
    void setSyncEnable(bool syncEnable);

public slots:
    virtual void read();
    virtual void synchronise();
    virtual void reset();
};

#endif // SETTINGS_HPP
