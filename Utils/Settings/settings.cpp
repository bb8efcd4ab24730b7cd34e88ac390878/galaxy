#include "settings.hpp"
#include <QMetaProperty>
#include <QDebug>

bool Settings::syncEnable() const
{
    return m_syncEnable;
}

void Settings::setSyncEnable(bool syncEnable)
{
    m_syncEnable = syncEnable;
}

Settings::Settings(QObject *parent) : QSettings(parent) {}

void Settings::read()
{
    qDebug() << endl << "Settings:";
    qDebug() << "File location:" << fileName();
    QMetaMethod synchroniseMethod = metaObject()->method(metaObject()->indexOfMethod(metaObject()->normalizedSignature("synchronise()")));
    for (int i = 0; i < metaObject()->propertyCount(); i++)
    {
        QMetaProperty property = metaObject()->property(i);

        property.write(this, value(property.name(), property.read(this)));

        connect(this, property.notifySignal(), this, synchroniseMethod);

        qDebug() << property.name() << ":" << property.read(this);
    }
    qDebug() << endl;
}

void Settings::synchronise()
{
    if (syncEnable())
    {
        for (int i = 0; i < metaObject()->propertyCount(); i++)
        {
            QMetaProperty property = metaObject()->property(i);

            if (QString("objectName") == property.name()) continue;

            if (property.isEnumType())
            {
                setValue(property.name(), property.enumerator().valueToKey(property.read(this).toInt()));
            }
            else
            {
                setValue(property.name(), property.read(this));
            }
        }
        sync();
//        qDebug() << QString("synchronise() invoked by signal: %1").arg(QString(metaObject()->method(senderSignalIndex()).methodSignature()));
    }
}

void Settings::reset()
{
    for (int i = 0; i < metaObject()->propertyCount(); i++)
    {
        QMetaProperty property = metaObject()->property(i);

        if (QString("objectName") == property.name()) continue;

        property.reset(this);
    }
}
