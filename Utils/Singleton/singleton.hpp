#ifndef SINGLETON_HPP
#define SINGLETON_HPP

#include <QObject>

template<typename T, typename std::enable_if<std::is_base_of<QObject, T>::value>::type* = nullptr>
class Singleton
{
    T *object;
    bool needToDelete;

public:
    Singleton(T *obj = nullptr) : object(obj), needToDelete(obj != nullptr)
    {
        if (needToDelete)
        {
            QObject::connect(object, &QObject::destroyed, [this](){
                needToDelete = false;
            });
        }
    }

    virtual ~Singleton() { if (needToDelete) object->deleteLater(); }

    T *instance() {
        if (!needToDelete)
        {
            object = new T();
            needToDelete = true;
            QObject::connect(object, &QObject::destroyed, [this](){
                needToDelete = false;
            });
        }
        return object;
    }

    T *operator -> () { return instance(); }
};

#endif // SINGLETON_HPP
